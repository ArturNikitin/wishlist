package com.svetka.wishilist.controller;

import com.svetka.wishilist.dto.BoardDto;
import com.svetka.wishilist.service.BoardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Log4j2
@CrossOrigin
@RestController
@RequiredArgsConstructor
@Api(tags = {"методы для работы с Board"})
public class BoardController {

    private final BoardService boardService;

    //TODO ADD TESTSSSSS
    @GetMapping("getBoard/{boardId}")
    @ApiOperation(value = "получение доски")
    public BoardDto getBoard(@PathVariable Long boardId) {
        log.debug("Received request to get a board with id {}", boardId);
        BoardDto board = boardService.getBoardById(boardId);
        log.debug("Finished request to get a board with data {}", board);
        return board;
    }

    //TODO ADD TESTSSSSS
    @PostMapping("user/{userId}/createBoard")
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "создание доски для пользователя")
    public BoardDto createBoard(@PathVariable Long userId,
                                @RequestBody BoardDto boardDto,
                                @RequestHeader("authorization") String auth) {
        log.debug("Received request to create board with data {}, for user with id {}" +
                " and auth token {}", boardDto, userId, auth);
        BoardDto board = boardService.createBoard(userId, boardDto, auth);
        log.debug("Finished request to create board with data {}, for user with id {}" +
                " and auth token {}", board, userId, auth);
        return board;
    }

    //TODO ADD TESTSSSSS
    @PostMapping("user/{userId}/updateBoard")
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    @ApiOperation(value = "изменение своей доски пользователем")
    public BoardDto updateBoard(@PathVariable Long userId,
                                @RequestBody BoardDto boardDto,
                                @RequestHeader("authorization") String auth) {
        log.debug("Received request to update board with data {}, for user with id {}" +
                " and auth token {}", boardDto, userId, auth);
        BoardDto board = boardService.updateBoard(userId, boardDto, auth);
        log.debug("Finished request to update board with data {}, for user with id {}" +
                " and auth token {}", board, userId, auth);
        return board;
    }

    @DeleteMapping("/user/{userId}/deleteBoard/{boardId}")
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation(value = "удаление пользователес своей доски по id")
    public void deleteBoard(@PathVariable Long userId,
                            @PathVariable Long boardId,
                            @RequestHeader("authorization") String auth) {
        log.debug("Received request to delete board with id {} and user id {} and auth token {}", boardId, userId, auth);
        boardService.deleteBoard(userId, boardId, auth);
        log.debug("Finished request to delete board with id {} and user id {} and auth token {}", boardId, userId, auth);

    }

    @GetMapping("/user/{userId}/getAllBoards")
    @ApiOperation(value = "отсортированные доски пользователя, дефолтно возвращается последние обновленные")
    //TODO ADD TESTSSSSS
    // TODO Убрать отображение приватных желаний и добавить другой метод только для своих желаний
    public List<BoardDto> getAllBoardsByUser(@PathVariable Long userId,
                                             @SortDefault(sort = {"lastModifiedDate"}, direction = Sort.Direction.DESC) Sort sort) {
        log.debug("Received request to get all boards by user with id {} and sort {}", userId, sort);
        List<BoardDto> boards = boardService.getSortedBoardsByUserId(userId, sort);
        log.debug("Finished request to get all boards by user with id {} and data [{}]", userId, boards);
        return boards;
    }
}
