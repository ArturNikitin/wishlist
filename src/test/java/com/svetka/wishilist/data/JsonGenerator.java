package com.svetka.wishilist.data;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.svetka.wishilist.dto.BoardDto;
import com.svetka.wishilist.dto.WishDto;
import com.svetka.wishilist.entity.Wish;
import com.svetka.wishilist.entity.enums.Access;
import lombok.SneakyThrows;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Класс для генерации json для тестов
 *
 * Сохранять свои json в пакет json с логичным названием меняя JSON_NAME
 * */

public class JsonGenerator {
    private static final String JSON_NAME = "creatBoard";
    @SneakyThrows
    public static void main(String[] args) {
        File file = new File("src/test/java/com/svetka/wishilist/data/json/" + JSON_NAME +".json");

        ObjectMapper mapper = new ObjectMapper();
        BoardDto boardDto = BoardDto.builder()
                .access("PUBLIC")
                .name("My Board")
                .isOccasion(false)
                .build();


        String value = mapper.writeValueAsString(boardDto);



        BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(file));
        out.write(value.getBytes());
        out.flush();
    }
}
