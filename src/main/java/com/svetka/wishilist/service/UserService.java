package com.svetka.wishilist.service;

import com.svetka.wishilist.dto.ImageDto;
import com.svetka.wishilist.dto.RegistrationUserDto;
import com.svetka.wishilist.dto.UserDto;

import java.util.List;


public interface UserService {
    UserDto getUserById(Long id);

    UserDto createUser(RegistrationUserDto userDto);

    UserDto updateUser(UserDto userDto);

    UserDto getUserByName(String username);

    List<UserDto> getAllUsers();

    UserDto blockUserById(Long id);

    UserDto unblockUserById(Long id);

    List<UserDto> getAllBlockedUsers();

    //    TODO delete user; users;
    boolean validateUserByIdAndJWT(Long userId, String auth);

    boolean validateUserOwnsWish(Long userId, Long wishId);

    boolean validateUserOwnsBoard(Long userId, Long boardId);

    UserDto addUserImage(Long userId, ImageDto imageDto, String auth);
}
