CREATE TABLE IF NOT EXISTS Boards
(
    id                 SERIAL PRIMARY KEY,
    created_date       TIMESTAMP   NOT NULL,
    last_modified_date TIMESTAMP   NOT NULL,
    name               VARCHAR(50) NOT NULL,
    is_occasion        BOOLEAN     NOT NULL,
    board_photo_id     BIGINT,
    access             VARCHAR(20) NOT NULL,
    CONSTRAINT wishlist_files_id_fk FOREIGN KEY (board_photo_id)
        REFERENCES wishlist_files (id)
)