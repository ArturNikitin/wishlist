package com.svetka.wishilist.entity.enums;

public enum UserRoles {
    ROLE_USER, ROLE_ADMIN
}
