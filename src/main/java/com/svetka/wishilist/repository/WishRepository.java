package com.svetka.wishilist.repository;

import com.svetka.wishilist.entity.Wish;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.Optional;

@RepositoryRestResource(exported = false)
public interface WishRepository extends JpaRepository<Wish, Long>, PagingAndSortingRepository<Wish, Long> {
    Optional<Wish> getById(Long wishId);

    List<Wish> findByUserId(Long userId, Sort sort);
}
