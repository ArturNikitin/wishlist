CREATE TABLE IF NOT EXISTS Categories
(
    id                SERIAL PRIMARY KEY,
    name              VARCHAR(50) NOT NULL UNIQUE,
    category_photo_id BIGINT,
    CONSTRAINT wishlist_files_id_fk FOREIGN KEY (category_photo_id)
        REFERENCES wishlist_files (id)
)