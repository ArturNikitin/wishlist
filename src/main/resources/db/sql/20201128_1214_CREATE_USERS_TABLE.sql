CREATE TABLE IF NOT EXISTS Users
(
    id                 SERIAL PRIMARY KEY,
    created_date       TIMESTAMP   NOT NULL,
    username           VARCHAR(50) NOT NULL UNIQUE,
    email              VARCHAR UNIQUE,
    password           VARCHAR     NOT NULL,
    isEnabled          BOOLEAN     NOT NULL,
    isAccountNonLocked BOOLEAN     NOT NULL,
    role               VARCHAR     NOT NULL,
    login              VARCHAR(50) UNIQUE,
    profile_photo_id   BIGINT,
    CONSTRAINT wishlist_files_id_fk FOREIGN KEY (profile_photo_id)
        REFERENCES wishlist_files (id)
)