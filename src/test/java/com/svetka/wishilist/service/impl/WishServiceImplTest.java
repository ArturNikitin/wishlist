package com.svetka.wishilist.service.impl;

import com.svetka.wishilist.repository.UserRepository;
import com.svetka.wishilist.repository.WishRepository;
import com.svetka.wishilist.service.UserService;
import com.svetka.wishilist.service.WishService;
import ma.glasnost.orika.MapperFacade;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;

class WishServiceImplTest {

    @Mock
    private WishRepository wishRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserService userService;

    @Mock
    private MapperFacade mapper;

    @InjectMocks
    private WishService wishService;

    @BeforeEach
    void setUp() {
    }

    @Test
    void getNumberOfWishesByUser() {
    }

    @Test
    void createWish() {
    }

    @Test
    void getWishById() {
    }

    @Test
    void deleteWishById() {
    }

    @Test
    void getSortedWishesByUserId() {
    }
}