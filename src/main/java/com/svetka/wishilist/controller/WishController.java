package com.svetka.wishilist.controller;

import com.svetka.wishilist.dto.ImageDto;
import com.svetka.wishilist.dto.WishDto;
import com.svetka.wishilist.service.WishService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;


@Log4j2
@CrossOrigin
@RestController
@RequiredArgsConstructor
@Api(tags = {"методы для работы с Wish"})
public class WishController {
    private final WishService wishService;

    @PostMapping(value = "/user/{userId}/createWish")
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    @ResponseStatus(HttpStatus.CREATED)
    //TODO ADD TESTSSSSS
    public WishDto createWish(@PathVariable Long userId,
                              @RequestBody WishDto wishDto,
                              @RequestHeader("authorization") String auth) {
        log.debug("Received request to create wish with data{} for user with id {}", wishDto, userId);
        WishDto wish = wishService.createWish(userId, wishDto, auth);
        log.debug("Finished request to create wish with data {}", wish);
        return wish;
    }

    @GetMapping("/getWish/{wishId}")
    //TODO ADD TESTSSSSS
    public WishDto getWish(@PathVariable Long wishId) {
        log.debug("Received request to get wish with id {}", wishId);
        WishDto wish = wishService.getWishById(wishId);
        log.debug("Finished request to get wish with data {}", wish);
        return wish;
    }

    @DeleteMapping("/user/{userId}/deleteWish/{wishId}")
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation(value = "удаление желания по id, можно удалить только свое желание")
    //TODO ADD TESTSSSSS
    public void deleteWishById(@PathVariable Long userId,
                               @PathVariable Long wishId,
                               @RequestHeader("authorization") String auth) {
        log.debug("Received request to delete wish with id {} for user with id {}", wishId, userId);
        wishService.deleteWishById(wishId, userId, auth);
        log.debug("Finished request to delete wish for user with id {}", userId);
    }

    @GetMapping("/user/{userId}/getAllWishes")
    @ApiOperation(value = "отсортированны желания пользователя, дефолтно возвращается последние обновленные")
    //TODO ADD TESTSSSSS
    // TODO Убрать отображение приватных желаний и добавить другой метод только для своих желаний
    public List<WishDto> getAllWishesByUser(@PathVariable Long userId,
                                            @SortDefault(sort = {"lastModifiedDate"}, direction = Sort.Direction.DESC) Sort sort) {
        log.debug("Received request for all wishes for user wish id {} and sort {}", userId, sort);
        List<WishDto> sortedWishesByUserId = wishService.getSortedWishesByUserId(userId, sort);
        log.debug("Finished request for all wishes for user with id {} and with data [{}]", userId, sortedWishesByUserId);
        return sortedWishesByUserId;
    }

    @PostMapping("/user/{userId}/updateWish")
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    @ResponseStatus(HttpStatus.ACCEPTED)
    @ApiOperation(value = "обновить свое желание")
    //TODO ADD TESTSSSSS
    public WishDto updateWish(@PathVariable Long userId,
                              @RequestBody WishDto wishDto,
                              @RequestHeader("authorization") String auth) {
        log.debug("Received request to update wish with data{} for user with id {} and auth token {}", wishDto, userId, auth);
        WishDto wish = wishService.updateWish(userId, wishDto, auth);
        log.debug("Finished request to update wish with data {}", wish);
        return wish;
    }

}
