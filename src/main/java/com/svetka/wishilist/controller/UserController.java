package com.svetka.wishilist.controller;

import com.svetka.wishilist.dto.UserDto;
import com.svetka.wishilist.repository.UserRepository;
import com.svetka.wishilist.service.UserService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Log4j2
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
@Api(tags = {"методы для работы с User"})
public class UserController {

    private final UserService userService;
    private final UserRepository userRepository;

    @GetMapping("/{userId}")
//    todo test
    public UserDto getUserById(@PathVariable Long userId) {
        log.debug("Requested to find user with id {}", userId);
        UserDto user = userService.getUserById(userId);
        log.debug("Finished requst to find user with id {} with data {}", userId, user);
        return user;
    }
    @GetMapping("/all")
    public List<UserDto> getAllUsers() {
       return userService.getAllUsers();
    }
}

