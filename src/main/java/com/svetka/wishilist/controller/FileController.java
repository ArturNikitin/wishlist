package com.svetka.wishilist.controller;

import com.svetka.wishilist.dto.ImageDto;
import com.svetka.wishilist.dto.UserDto;
import com.svetka.wishilist.entity.FileEntity;
import com.svetka.wishilist.repository.FileEntityRepository;
import com.svetka.wishilist.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;
import static org.springframework.http.MediaType.parseMediaType;

@Log4j2
@CrossOrigin
@RestController
@RequiredArgsConstructor
@Api(tags = {"методы для работы с картинками"})
public class FileController {
    private final UserService userService;
    private final FileEntityRepository fileEntityRepository;

    @PostMapping(consumes = {MULTIPART_FORM_DATA_VALUE}, path = "/user/{userId}/addImage")
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    @ApiOperation(value = "добавить аватар пользователю")
    public UserDto addUserImage(@PathVariable Long userId,
                                @RequestHeader("authorization") String auth,
                                @RequestPart("file") MultipartFile file) {

        final ImageDto imageDto = buildImageDtoFromMultipartFile(file);
        log.info("Request to add user image with file name {}", imageDto.getName());
        UserDto userDto = userService.addUserImage(userId, imageDto, auth);
        log.info("Finished request to add user image with date [{}]", userDto);
        return userDto;
    }

    @GetMapping(value = "getImage/{imageId}", produces = MediaType.IMAGE_JPEG_VALUE)
    public @ResponseBody byte[] getImage(@PathVariable Long imageId) {
        final FileEntity fileEntity = fileEntityRepository.findById(imageId).orElseThrow();
        return fileEntity.getData();
    }

    @GetMapping(value = "getAnyImage/{imageId}")
    public ResponseEntity<byte []> getAnyImage(@PathVariable Long imageId) {
        final FileEntity fileEntity = fileEntityRepository.findById(imageId).orElseThrow();
        return ResponseEntity.ok()
                .contentLength(fileEntity.getData().length)
                .contentType(parseMediaType(fileEntity.getFileType()))
                .body(fileEntity.getData());
    }

    @GetMapping(value = "getImageDto/{imageId}")
    public ImageDto getImageDto(@PathVariable Long imageId) {
        final FileEntity fileEntity = fileEntityRepository.findById(imageId).orElseThrow();
        return ImageDto.builder().id(fileEntity.getId())
                .name(fileEntity.getFileName())
                .fileType(fileEntity.getFileType())
                .data(fileEntity.getData())
                .build();
    }

    @SneakyThrows
    private ImageDto buildImageDtoFromMultipartFile(MultipartFile file) {
        return ImageDto.builder()
                .data(file.getBytes())
                .name(file.getOriginalFilename())
                .fileType(file.getContentType())
                .size(file.getSize())
                .build();
    }
}
