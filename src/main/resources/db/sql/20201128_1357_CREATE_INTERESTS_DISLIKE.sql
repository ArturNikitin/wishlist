CREATE TABLE IF NOT EXISTS Interests_dislike
(
    id          SERIAL PRIMARY KEY,
    user_id     BIGINT NOT NULL,
    interest_id BIGINT NOT NULL,
    CONSTRAINT user_id_fk FOREIGN KEY (user_id)
        REFERENCES users (id),
    CONSTRAINT interests_id FOREIGN KEY (interest_id)
        REFERENCES interests (id)
)