package com.svetka.wishilist.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ImageDto {

    @JsonProperty
    private Long id;

    @JsonProperty(value = "fileName")
    private String name;

    @JsonProperty
    private String fileType;

    @JsonProperty
    private byte[] data;

    @JsonProperty
    private long size;
}
