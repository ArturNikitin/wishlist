package com.svetka.wishilist.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.svetka.wishilist.entity.Category;
import com.svetka.wishilist.entity.FileEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    @JsonProperty
    private Long id;

    @JsonProperty
    private String username;

    @JsonProperty
    private String email;

    @JsonProperty
    private String login;

    @JsonProperty
    private String role;

    @JsonProperty
    private ImageDto image;

    @JsonProperty
    private String createdDate;

    @JsonProperty
    private List<InterestDto> likes;

    @JsonProperty
    private List<InterestDto> dislikes;

    @JsonProperty
    private List<CategoryDto> categories;
}
