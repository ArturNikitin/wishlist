ALTER TABLE boards ADD COLUMN user_id BIGINT;
ALTER TABLE boards ADD FOREIGN KEY (user_id)
    REFERENCES users (id);
ALTER TABLE wishes alter board_id DROP NOT NULL;