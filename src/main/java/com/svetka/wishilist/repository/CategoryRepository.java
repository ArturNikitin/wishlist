package com.svetka.wishilist.repository;

import com.svetka.wishilist.entity.Category;
import io.swagger.annotations.Api;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@Repository
@Api( tags = "методы для работы с Category")
public interface CategoryRepository extends JpaRepository<Category, Long> {
}
