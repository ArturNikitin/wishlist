package com.svetka.wishilist.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WishDto {
    @JsonProperty
    private Long id;

    @JsonProperty
    private String name;

    @JsonProperty("booked")
    private boolean isBooked;

    @JsonProperty("analogPossible")
    private boolean isAnalogPossible;

    @JsonProperty
    private String access;

    @JsonProperty
    private String location;

    @JsonProperty
    private String details;

    @JsonProperty
    private String link;

    @JsonProperty
    private BigDecimal price;

    @JsonProperty
    private String createdDate;

    @JsonProperty
    private String lastModifiedDate;

    @JsonProperty
    private ImageDto image;
}
