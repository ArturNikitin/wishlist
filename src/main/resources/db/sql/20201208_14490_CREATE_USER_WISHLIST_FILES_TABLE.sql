CREATE TABLE IF NOT EXISTS User_wishlist_files
(
    id        SERIAL PRIMARY KEY,
    data      BYTEA   NOT NULL,
    file_type VARCHAR NOT NULL,
    user_id   BIGINT UNIQUE,
    board_id  BIGINT UNIQUE,
    wish_id   BIGINT UNIQUE,
        CONSTRAINT user_id_fk FOREIGN KEY (user_id)
        REFERENCES users (id),
    CONSTRAINT board_id_fk FOREIGN KEY (board_id)
        REFERENCES boards (id),
    CONSTRAINT wish_id_fk FOREIGN KEY (wish_id)
        REFERENCES wishes (id)
)