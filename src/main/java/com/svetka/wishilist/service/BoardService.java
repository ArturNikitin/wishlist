package com.svetka.wishilist.service;

import com.svetka.wishilist.dto.BoardDto;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface BoardService {
    BoardDto getBoardById(Long boardId);

    BoardDto createBoard(Long userId, BoardDto boardDto, String auth);

    BoardDto updateBoard(Long userId, BoardDto boardDto, String auth);

    List<BoardDto> getSortedBoardsByUserId(Long userId, Sort sort);

    void deleteBoard(Long userId, Long boardId, String auth);
}
