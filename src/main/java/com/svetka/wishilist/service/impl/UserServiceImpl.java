package com.svetka.wishilist.service.impl;

import com.svetka.wishilist.dto.ImageDto;
import com.svetka.wishilist.dto.RegistrationUserDto;
import com.svetka.wishilist.dto.UserDto;
import com.svetka.wishilist.entity.Board;
import com.svetka.wishilist.entity.FileEntity;
import com.svetka.wishilist.entity.User;
import com.svetka.wishilist.entity.Wish;
import com.svetka.wishilist.entity.enums.UserRoles;
import com.svetka.wishilist.exception.EntityNotFoundException;
import com.svetka.wishilist.repository.UserRepository;
import com.svetka.wishilist.service.UserService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import lombok.RequiredArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.crypto.SecretKey;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final MapperFacade mapper;
    private final PasswordEncoder encoder;
    private final SecretKey secretKey;

    @Override
    @Transactional
    public UserDto getUserById(Long id) {
        User user = userRepository.getUserById(id).orElseThrow(() -> new EntityNotFoundException("User not found", id.toString()));
        return getUserDto(user);
    }

    @Override
    @Transactional
    public UserDto createUser(RegistrationUserDto userDto) {
        User user = mapper.map(userDto, User.class);
        user.setRole(UserRoles.ROLE_USER);
        user.setPassword(encoder.encode(userDto.getPassword()));
        user.setEnabled(true);
        user.setAccountNonLocked(true);
        user.setCreatedDate(LocalDateTime.now());
        return getUserDto(userRepository.save(user));
    }

    @Override
    @Transactional
    public UserDto updateUser(UserDto userDto) {

        return null;
    }

    @Override
    @Transactional
    public UserDto getUserByName(String username) {
        return null;
    }

    @Override
    @Transactional
    public List<UserDto> getAllUsers() {
        return userRepository.findAll().stream().map(user ->  mapper.map(user, UserDto.class)).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public UserDto blockUserById(Long id) {
        return null;
    }

    @Override
    @Transactional
    public UserDto unblockUserById(Long id) {
        return null;
    }

    @Override
    @Transactional
    public List<UserDto> getAllBlockedUsers() {
        return null;
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public boolean validateUserByIdAndJWT(Long userId, String principal) {
        User userById = userRepository.getUserById(userId)
                .orElseThrow(() -> new EntityNotFoundException("User not found", userId.toString()));

        return userById.getUsername().equals(getUsernameFromJWT(principal));
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public boolean validateUserOwnsWish(Long userId, Long wishId) {
        User userById = userRepository.getUserById(userId)
                .orElseThrow(() -> new EntityNotFoundException("User not found", userId.toString()));

        return userById.getWishes().stream().map(Wish::getId).anyMatch(id -> id.equals(wishId));
    }
    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public boolean validateUserOwnsBoard(Long userId, Long boardId) {
        User userById = userRepository.getUserById(userId)
                .orElseThrow(() -> new EntityNotFoundException("User not found", userId.toString()));

        return userById.getBoards().stream().map(Board::getId).anyMatch(id -> id.equals(boardId));
    }

    @Override
    @Transactional
    public UserDto addUserImage(Long userId, ImageDto imageDto, String auth) {
        final boolean isUserValid = validateUserByIdAndJWT(userId, auth);

        if (isUserValid) {
            final User user = userRepository.findById(userId)
                    .orElseThrow(() -> new EntityNotFoundException("User not found", userId.toString()));
            user.setProfilePhoto(mapper.map(imageDto, FileEntity.class));
            return getUserDto(userRepository.save(user));
        } else
        throw new UsernameNotFoundException(String.format("User with id %d is not the same user as token %s says", userId, auth));
    }

    private UserDto getUserDto(User user) {
        return mapper.map(user, UserDto.class);
    }

    private String getUsernameFromJWT(String principal) {
        try {
            Jws<Claims> claimsJws = Jwts.parserBuilder()
                    .setSigningKey(secretKey)
                    .build().parseClaimsJws(principal.replace("Bearer ", ""));

            return claimsJws.getBody().getSubject();
        } catch (JwtException e) {
            throw new IllegalStateException(String.format("Token {%s} cannot be trusted", principal));
        }
    }
}
