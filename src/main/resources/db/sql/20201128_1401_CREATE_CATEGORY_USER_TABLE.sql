CREATE TABLE IF NOT EXISTS Category_user
(
    id          SERIAL PRIMARY KEY,
    user_id     BIGINT NOT NULL,
    category_id BIGINT NOT NULL,
    CONSTRAINT user_id_fk FOREIGN KEY (user_id)
        REFERENCES users (id),
    CONSTRAINT interests_id FOREIGN KEY (category_id)
        REFERENCES Categories (id)
)