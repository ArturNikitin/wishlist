package com.svetka.wishilist.service.impl;

import com.svetka.wishilist.dto.WishDto;
import com.svetka.wishilist.entity.CustomFileEntity;
import com.svetka.wishilist.entity.Wish;
import com.svetka.wishilist.exception.EntityNotFoundException;
import com.svetka.wishilist.repository.FileEntityRepository;
import com.svetka.wishilist.repository.UserRepository;
import com.svetka.wishilist.repository.WishRepository;
import com.svetka.wishilist.service.UserService;
import com.svetka.wishilist.service.WishService;
import lombok.RequiredArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class WishServiceImpl implements WishService {
    private final WishRepository wishRepository;
    private final UserRepository userRepository;
    private final UserService userService;
    private final MapperFacade mapper;
    private final FileEntityRepository fileEntityRepository;

    @Override
    public Long getNumberOfWishesByUser(Long userId) {
        return null;
    }

    @Override
    @Transactional
//    todo test
    public WishDto createWish(Long userId, WishDto wishDto, String principal) {
        boolean validated = userService.validateUserByIdAndJWT(userId, principal);
        if (validated) {
            Wish wish = mapper.map(wishDto, Wish.class);
            wish.setUser(userRepository.getUserById(userId).orElseThrow(() ->
                    new EntityNotFoundException("User not found", userId.toString())
            ));
            wish.setCreatedDate(LocalDateTime.now());
            wish.setLastModifiedDate(LocalDateTime.now());
            wish.setPhoto(fileEntityRepository.findById(4L).orElseThrow());
            return getWishDto(wishRepository.save(wish));
        } else {
            throw new UsernameNotFoundException(String.format("User with id %d is not the same user as token %s says", userId, principal));
        }
    }

    @Override
    @Transactional
    //    todo test
    public WishDto getWishById(Long wishId) {
        return getWishDto(wishRepository.getById(wishId).orElseThrow(() ->
                new EntityNotFoundException("Wish not found", wishId.toString())
        ));
    }

    @Override
    @Transactional
    //    todo test
    public void deleteWishById(Long wishId, Long userId, String auth) {
        boolean validated = userService.validateUserByIdAndJWT(userId, auth);
        if (validated) {
            wishRepository.deleteById(wishId);
        } else {
            throw new UsernameNotFoundException(String.format("User with id %d is not the same user as token %s says", userId, auth));
        }
    }

    @Override
    @Transactional
    //    todo test
    public List<WishDto> getSortedWishesByUserId(Long userId, Sort sort) {
        return wishRepository.findByUserId(userId, sort).stream()
                .map(wish -> mapper.map(wish, WishDto.class))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
//    todo add tests
    public WishDto updateWish(Long userId, WishDto wishDto, String auth) {
        boolean validatedUser = userService.validateUserByIdAndJWT(userId, auth);
        boolean validatedWish = userService.validateUserOwnsWish(userId, wishDto.getId());
        if (validatedUser && validatedWish) {
            Wish wish = mapper.map(wishDto, Wish.class);
            wish.setLastModifiedDate(LocalDateTime.now());
            wish.setUser(userRepository.getUserById(userId).orElseThrow(() -> new EntityNotFoundException("User not found", userId.toString())));
            return getWishDto(wishRepository.save(wish));
        } else {
            throw new UsernameNotFoundException(String.format("User with id %d is not the same user as token %s says", userId, auth));
        }
    }

    private WishDto getWishDto(Wish wish) {
        return mapper.map(wish, WishDto.class);
    }
}
