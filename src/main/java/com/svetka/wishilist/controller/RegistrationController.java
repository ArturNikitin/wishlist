package com.svetka.wishilist.controller;

import com.svetka.wishilist.dto.RegistrationUserDto;
import com.svetka.wishilist.dto.UserDto;
import com.svetka.wishilist.service.UserService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@CrossOrigin
@RestController
@RequiredArgsConstructor
public class RegistrationController {
    private final UserService userService;

    @PostMapping("/registration")
    @ApiOperation(value = "Create User", tags = {"REGISTRATION"})
    //TODO ADD TESTSSSSS
    public UserDto register(@RequestBody RegistrationUserDto userDto) {
        log.debug("Received request to create User with data {}", userDto);
        UserDto user = userService.createUser(userDto);
        log.debug("Finished request to create user with Data {}", user);
        return user;
    }
}
