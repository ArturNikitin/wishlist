package com.svetka.wishilist.config;

import com.svetka.wishilist.dto.BoardDto;
import com.svetka.wishilist.dto.CategoryDto;
import com.svetka.wishilist.dto.ImageDto;
import com.svetka.wishilist.dto.InterestDto;
import com.svetka.wishilist.dto.UserDto;
import com.svetka.wishilist.dto.WishDto;
import com.svetka.wishilist.entity.Board;
import com.svetka.wishilist.entity.Category;
import com.svetka.wishilist.entity.CustomFileEntity;
import com.svetka.wishilist.entity.FileEntity;
import com.svetka.wishilist.entity.User;
import com.svetka.wishilist.entity.Wish;
import lombok.SneakyThrows;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import net.rakugakibox.spring.boot.orika.OrikaMapperFactoryConfigurer;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.stream.Collectors;


@Component
public class MapperConfig implements OrikaMapperFactoryConfigurer {

    private static final DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

    @Override
    public void configure(MapperFactory factory) {
        factory.classMap(Board.class, BoardDto.class)
                .byDefault()
                .customize(new CustomMapper<Board, BoardDto>() {
                    @Override
                    public void mapAtoB(Board board, BoardDto boardDto, MappingContext context) {
                        boardDto.setCreatedDate(board.getCreatedDate().format(format));
                        boardDto.setLastModifiedDate(board.getLastModifiedDate().format(format));
                        if (board.getCustomImage() != null)
                            boardDto.setImage(buildImageDtoFromCustomFileEntity(board.getCustomImage()));
                        else
                            boardDto.setImage(buildImageDtoFromFileEntity(board.getPhoto()));
                    }

                    @Override
                    public void mapBtoA(BoardDto boardDto, Board board, MappingContext context) {
                        if (boardDto.getCreatedDate() != null)
                            board.setCreatedDate(LocalDateTime.parse(boardDto.getCreatedDate(), format));
                        if (boardDto.getLastModifiedDate() != null)
                            board.setLastModifiedDate(LocalDateTime.parse(boardDto.getLastModifiedDate(), format));
                    }
                })
                .register();


        factory.classMap(User.class, UserDto.class)
                .byDefault()
                .customize(new CustomMapper<User, UserDto>() {
                    @Override
                    public void mapAtoB(User user, UserDto userDto, MappingContext context) {
                        userDto.setCreatedDate(user.getCreatedDate().format(format));
                        if (user.getCustomImage() != null)
                            userDto.setImage(buildImageDtoFromCustomFileEntity(user.getCustomImage()));
                        else
                            userDto.setImage(buildImageDtoFromFileEntity(user.getProfilePhoto()));
                        userDto.setLikes(user.getInterestsLike().stream()
                                .map(like -> mapperFacade.map(like, InterestDto.class))
                                .collect(Collectors.toList()));
                        userDto.setDislikes(user.getInterestsDislike().stream()
                                .map(dislike -> mapperFacade.map(dislike, InterestDto.class))
                                .collect(Collectors.toList()));
                        userDto.setCategories(user.getCategories().stream()
                                .map(category -> mapperFacade.map(category, CategoryDto.class))
                                .collect(Collectors.toList()));
                    }

                    @Override
                    public void mapBtoA(UserDto userDto, User user, MappingContext context) {
                        user.setCreatedDate(LocalDateTime.parse(userDto.getCreatedDate(), format));
                    }
                })
                .register();


        factory.classMap(Wish.class, WishDto.class)
                .exclude("createdDate")
                .exclude("lastModifiedDate")
                .byDefault()
                .customize(new CustomMapper<Wish, WishDto>() {
                    @Override
                    public void mapAtoB(Wish wish, WishDto wishDto, MappingContext context) {
                        wishDto.setCreatedDate(wish.getCreatedDate().format(format));
                        wishDto.setLastModifiedDate(wish.getLastModifiedDate().format(format));
                        if (wish.getCustomImage() != null)
                            wishDto.setImage(buildImageDtoFromCustomFileEntity(wish.getCustomImage()));
                        else
                            wishDto.setImage(buildImageDtoFromFileEntity(wish.getPhoto()));
                    }

                    @Override
                    public void mapBtoA(WishDto wishDto, Wish wish, MappingContext context) {
                        if (wishDto.getCreatedDate() != null)
                            wish.setCreatedDate(LocalDateTime.parse(wishDto.getCreatedDate(), format));
                        if (wishDto.getLastModifiedDate() != null)
                            wish.setLastModifiedDate(LocalDateTime.parse(wishDto.getLastModifiedDate(), format));
                    }
                }).register();

        factory.classMap(FileEntity.class, ImageDto.class)
                .field("fileName", "name")
                .byDefault()
                .register();

        factory.classMap(ImageDto.class, FileEntity.class)
                .field("name", "fileName")
                .byDefault()
                .register();
    }

    //TODO add bytes
    @SneakyThrows
    private ImageDto buildImageDtoFromFileEntity(FileEntity file) {
        return ImageDto.builder()
                .id(file.getId())
                .name(file.getFileName())
                .fileType(file.getFileType())
                .build();
    }

    //TODO add bytes
    @SneakyThrows
    private ImageDto buildImageDtoFromCustomFileEntity(CustomFileEntity file) {
        return ImageDto.builder()
                .id(file.getId())
                .fileType(file.getFileType())
                .build();
    }
}

