package com.svetka.wishilist.repository;

import com.svetka.wishilist.entity.Board;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(exported = false)
public interface BoardRepository extends JpaRepository<Board, Long> {
    List<Board> findByUserId(Long id, Sort sort);
}
