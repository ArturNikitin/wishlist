package com.svetka.wishilist.service.impl;

import com.svetka.wishilist.dto.BoardDto;
import com.svetka.wishilist.entity.Board;
import com.svetka.wishilist.exception.EntityNotFoundException;
import com.svetka.wishilist.repository.BoardRepository;
import com.svetka.wishilist.repository.FileEntityRepository;
import com.svetka.wishilist.repository.UserRepository;
import com.svetka.wishilist.service.BoardService;
import com.svetka.wishilist.service.UserService;
import lombok.RequiredArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BoardServiceImpl implements BoardService {

    private final UserService userService;
    private final MapperFacade mapperFacade;
    private final BoardRepository boardRepository;
    private final UserRepository userRepository;
    private final FileEntityRepository fileEntityRepository;

    @Override
    @Transactional
    public BoardDto getBoardById(Long boardId) {
        return getBoardDto(boardRepository.findById(boardId)
                .orElseThrow(() ->
                        new EntityNotFoundException("Board not found", boardId.toString())));
    }

    @Override
    @Transactional
    public BoardDto createBoard(Long userId, BoardDto boardDto, String auth) {
        final boolean validatedUser = userService.validateUserByIdAndJWT(userId, auth);
        if (validatedUser) {
            Board board = mapperFacade.map(boardDto, Board.class);
            board.setCreatedDate(LocalDateTime.now());
            board.setLastModifiedDate(LocalDateTime.now());
            board.setPhoto(fileEntityRepository.findById(4L).orElseThrow());
            board.setUser(userRepository.findById(userId)
                    .orElseThrow(() -> new EntityNotFoundException("User not found", userId.toString())));
            return getBoardDto(boardRepository.save(board));
        } else
            throw new UsernameNotFoundException(String.format("User with id %d is not the same user as token %s says", userId, auth));
    }

    @Override
    @Transactional
    public BoardDto updateBoard(Long userId, BoardDto boardDto, String auth) {
        final boolean validatedUser = userService.validateUserByIdAndJWT(userId, auth);
        final boolean validateUserOwnsBoard = userService.validateUserOwnsBoard(userId, boardDto.getId());
        if (validatedUser && validateUserOwnsBoard) {
            Board board = mapperFacade.map(boardDto, Board.class);
            board.setLastModifiedDate(LocalDateTime.now());
            board.setUser(userRepository.findById(userId)
                    .orElseThrow(() -> new EntityNotFoundException("User not found", userId.toString())));
            return getBoardDto(boardRepository.save(board));
        } else
            throw new UsernameNotFoundException(String.format("User with id %d is not the same user as token %s says", userId, auth));
    }

    @Override
    @Transactional
    public List<BoardDto> getSortedBoardsByUserId(Long userId, Sort sort) {
        return boardRepository.findByUserId(userId, sort)
                .stream()
                .map(board
                        -> mapperFacade.map(board, BoardDto.class))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void deleteBoard(Long userId, Long boardId, String auth) {
        final boolean validatedUser = userService.validateUserByIdAndJWT(userId, auth);
        if (validatedUser) {
           boardRepository.deleteById(boardId);
        } else {
            throw new UsernameNotFoundException(String.format("User with id %d is not the same user as token %s says", userId, auth));
        }
    }

    private BoardDto getBoardDto(Board board) {
        return mapperFacade.map(board, BoardDto.class);
    }
}
