CREATE TABLE IF NOT EXISTS Interests_like
(
    id       SERIAL PRIMARY KEY,
    user1_id BIGINT      NOT NULL,
    user2_id BIGINT      NOT NULL,
    status   VARCHAR(50) NOT NULL,
    CONSTRAINT user_id_fk FOREIGN KEY (user1_id)
        REFERENCES users (id),
    CONSTRAINT interests_id FOREIGN KEY (user2_id)
        REFERENCES users (id)
)