package com.svetka.wishilist.repository;

import com.svetka.wishilist.entity.Interest;
import io.swagger.annotations.Api;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@Repository
@Api(tags = "Методы для работы с Interest")
public interface InterestRepository extends JpaRepository<Interest, Long> {
}
