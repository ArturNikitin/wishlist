package com.svetka.wishilist.exception;

import lombok.Getter;

@Getter
public abstract class CustomException extends RuntimeException {

    private final String[] args;

    protected CustomException(String message, String ... args) {
        super(message);
        this.args = args;
    }
}
