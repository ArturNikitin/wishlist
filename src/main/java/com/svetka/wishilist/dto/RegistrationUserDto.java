package com.svetka.wishilist.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegistrationUserDto {
    @JsonProperty
    private Long id;

    @JsonProperty
    private String username;

    @JsonProperty
    @ToString.Exclude
    private String password;

    @JsonProperty
    private String email;

    @JsonProperty
    private String login;

    @JsonProperty
    private String role;
}
