package com.svetka.wishilist.repository;

import com.svetka.wishilist.entity.FileEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(exported = false)
public interface FileEntityRepository extends JpaRepository<FileEntity, Long> {
}
