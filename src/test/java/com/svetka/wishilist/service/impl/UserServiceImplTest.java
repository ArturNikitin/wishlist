package com.svetka.wishilist.service.impl;

import com.svetka.wishilist.dto.RegistrationUserDto;
import com.svetka.wishilist.dto.UserDto;
import com.svetka.wishilist.entity.User;
import com.svetka.wishilist.entity.enums.UserRoles;
import com.svetka.wishilist.exception.EntityNotFoundException;
import com.svetka.wishilist.repository.UserRepository;
import ma.glasnost.orika.MapperFacade;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class UserServiceImplTest {


    @Mock
    private MapperFacade mapper;

    @Mock
    private UserRepository userRepository;

    @Mock
    private PasswordEncoder encoder;

    @InjectMocks
    private UserServiceImpl userService;

    private final UserDto user = getUserDto();
    private final RegistrationUserDto registrationUserDto = getRegistrationUser();
    private final User userReceived = getUserReceived();
    private final User userToRegister = getUserToRegister();
    private final User userToSave = getUserToSave();
    private final User savedUser = getSavedUser();

    private final String PASSWORD = "VERY_SECURED_PASSWORD";

    @BeforeEach
    void setUp() {
    }

    @Test
    void getUserByIdTest() {
        when(userRepository.getUserById(1L)).thenReturn(Optional.of(userReceived));
        when(mapper.map(userReceived, UserDto.class)).thenReturn(user);

        UserDto userById = userService.getUserById(1L);

        assertNotNull(userById.getId());
        assertEquals(1L, userById.getId());
        assertEquals(user.getRole(), userById.getRole());
        assertEquals(user.getUsername(), userById.getUsername());
        verify(mapper, times(1)).map(userReceived, UserDto.class);
        verify(userRepository, times(1)).getUserById(1L);

    }

    @Test
    void getUserByIdUserNotFoundTest() {
        when(userRepository.getUserById(1L)).thenReturn(Optional.empty());

        EntityNotFoundException entityNotFoundException = assertThrows(EntityNotFoundException.class, () -> userService.getUserById(1L));

        assertEquals("User not found", entityNotFoundException.getMessage());
        assertEquals("1", entityNotFoundException.getArgs()[0]);
    }

    @Test
    void createUserTestSuccess() {
        when(mapper.map(registrationUserDto, User.class)).thenReturn(userToRegister);
        when(encoder.encode(registrationUserDto.getPassword())).thenReturn(PASSWORD);
        when(userRepository.save(userToSave)).thenReturn(savedUser);
        when(mapper.map(savedUser, UserDto.class)).thenReturn(user);

        UserDto createdUser = userService.createUser(registrationUserDto);

        verify(encoder, times(1)).encode("1234");
        verify(mapper, times(1)).map(registrationUserDto, User.class);
        verify(userRepository, times(1)).save(userToSave);
        verify(mapper, times(1)).map(savedUser, UserDto.class);
        assertEquals(1L, createdUser.getId());
        assertEquals("user", createdUser.getUsername());
        assertEquals("ROLE_USER", createdUser.getRole());
    }

    private User getUserReceived() {
        return User.builder()
                .id(1L)
                .role(UserRoles.ROLE_USER)
                .username("user")
                .password("1234")
                .build();
    }

    private UserDto getUserDto() {
        return UserDto.builder()
                .id(1L)
                .role("ROLE_USER")
                .username("user")
                .build();
    }

    private RegistrationUserDto getRegistrationUser() {
        return RegistrationUserDto.builder()
                .username("user")
                .password("1234")
                .build();

    }

    private User getUserToRegister() {
        return User.builder()
                .username("user")
                .password("1234")
                .build();
    }

    private User getUserToSave() {
        return User.builder()
                .username("user")
                .password(PASSWORD)
                .role(UserRoles.ROLE_USER)
                .isEnabled(true)
                .isAccountNonLocked(true)
                .build();
    }

    private User getSavedUser() {
        return User.builder()
                .id(1L)
                .username("user")
                .password(PASSWORD)
                .role(UserRoles.ROLE_USER)
                .isEnabled(true)
                .isAccountNonLocked(true)
                .build();
    }
}