package com.svetka.wishilist.entity.enums;

public enum Access {
    PRIVATE, PUBLIC
}
