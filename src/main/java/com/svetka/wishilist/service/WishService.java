package com.svetka.wishilist.service;

import com.svetka.wishilist.dto.WishDto;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface WishService {
    Long getNumberOfWishesByUser(Long userId);

    WishDto createWish(Long userId, WishDto wishDto, String principal);

    WishDto getWishById(Long wishId);

    void deleteWishById(Long wishId, Long userId, String principal);

    List<WishDto> getSortedWishesByUserId(Long userId, Sort sort);

    WishDto updateWish(Long userId, WishDto wishDto, String auth);
}
