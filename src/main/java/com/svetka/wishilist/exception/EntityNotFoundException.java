package com.svetka.wishilist.exception;

public class EntityNotFoundException extends CustomException {

    public EntityNotFoundException(String description, String ... args) {
        super(description, args);
    }
}
