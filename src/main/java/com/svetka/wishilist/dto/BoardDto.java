package com.svetka.wishilist.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BoardDto {

    @JsonProperty
    private Long id;

    @JsonProperty
    private String name;

    @JsonProperty()
    private String createdDate;

    @JsonProperty
    private String lastModifiedDate;

    @JsonProperty
    private String access;

    @JsonProperty("is_occasion")
    private boolean isOccasion;

    @JsonProperty
    private ImageDto image;
}
