package com.svetka.wishilist.security.service;

import com.svetka.wishilist.entity.User;
import com.svetka.wishilist.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserDetailServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.getUserByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException(String.format("User with [username] %s not found", username)));
        MyUserDetails myUserDetails = new MyUserDetails(user.getUsername(), user.getPassword(), user.getRole().toString());
        return myUserDetails;
    }
}
